import httplib
import re
import smtplib
from email.MIMEText import MIMEText
from email.MIMEMultipart import MIMEMultipart
from email.Utils import formatdate


def create_message(from_addr, to_addr, subject, body, files = []):
     msg = MIMEMultipart()
     msg['Subject'] = subject
     msg['From'] = from_addr
     msg['To'] = to_addr
     msg['Date'] = formatdate()
     content = MIMEText(body, 'html')
     msg.attach(content)

     # Attach a file
     for file in files:
          part = MIMEBase('application', "octet-stream")
          part.set_payload(open(file, "rb").read())           
          Encoders.encode_base64(part)
          part.add_header('Content-Disposition', 'attachment; filename="%s"' % os.path.basename(file))
          msg.attach(part)

     return msg


def getGlobalIp():
     # conn = httplib.HTTPConnection('ipcheck.ieserver.net',80,timeout=10)
     conn = httplib.HTTPConnection('checkip.dyndns.org',80,timeout=10)
     conn.request("GET","")
     r1 = conn.getresponse()
     data1 = r1.read()
     p = re.compile(r'<.*?>')
     str1 = p.sub('', data1)
     return str1


def send_via_gmail(from_addr, to_addr, msg):
     s = smtplib.SMTP('smtp.gmail.com', 587)
     s.ehlo()
     s.starttls()
     s.ehlo()
     s.login('yourGmailAddress', 'yourGmailPassword')
     s.sendmail(from_addr, [to_addr], msg.as_string())
     s.close()


if __name__ == '__main__':
     from_addr = 'yourGmailAddress'
     to_addr = 'your.Gmail.Address'
     body = getGlobalIp()     
     msg = create_message(from_addr, to_addr, 'Global IP', body)
     send_via_gmail(from_addr, to_addr, msg)

